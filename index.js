import React, {Component} from "react";
import {requireNativeComponent, NativeModules} from "react-native";

const RCTFace = requireNativeComponent('RCTFace', Face);

class Face extends Component {
    constructor(props) {
        super(props);
    }

    render() {
        return (
            <RCTFace {...this.props}>
                {this.props.children}
            </RCTFace>
        );
    }
}

export default Face;
