#import <UIKit/UIKit.h>
#import <AVFoundation/AVFoundation.h>
#import "../../../ios/Pods/AffdexSDK-iOS/ios/Affdex.framework/Headers/Affdex.h"
#import "React/RCTComponent.h"

@interface RCTFace : UIView<AFDXDetectorDelegate>
    @property (nonatomic, copy) RCTBubblingEventBlock onChange;
@end
