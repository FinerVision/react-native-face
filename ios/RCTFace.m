#import "RCTFace.h"
#import <AVFoundation/AVFoundation.h>
#import <CoreAudioKit/CoreAudioKit.h>
#import <CoreGraphics/CoreGraphics.h>

@interface RCTFace ()

@end

@implementation RCTFace

UIImageView *cameraView;
AFDXDetector *detector;

bool showCameraProp = true;

#pragma mark -
#pragma mark Convenience Methods

// This is a convenience method that is called by the detector:hasResults:forImage:atTime: delegate method below.
// You will want to do something with the face (or faces) found.
- (void)processedImageReady:(AFDXDetector *)detector image:(UIImage *)image faces:(NSDictionary *)faces atTime:(NSTimeInterval)time;
{
    // iterate on the values of the faces dictionary
    for (AFDXFace *face in [faces allValues])
    {
        // Here's where you actually "do stuff" with the face object (e.g. examine the emotions, expressions,
        // emojis, and other metrics).
        //NSLog(@"%@", face);
        // TODO: Parse the face object into something usable.
        NSString *emotions = [NSString stringWithFormat:@"%@", face.emotions];
        NSString *facePoints = [NSString stringWithFormat:@"%@", face.facePoints];
        _onChange(
            @{
              @"emotions": emotions,
              @"facePoints": facePoints
            }
        );
    }
}

- (void)setShowCamera:(BOOL)showCamera
{
    showCameraProp = showCamera;
    [cameraView setHidden:showCamera];
}


// This is a convenience method that is called by the detector:hasResults:forImage:atTime: delegate method below.
// It handles all UNPROCESSED images from the detector. Here I am displaying those images on the camera view.
- (void)unprocessedImageReady:(AFDXDetector *)detector image:(UIImage *)image atTime:(NSTimeInterval)time;
{
    // UI work must be done on the main thread, so dispatch it there.
    dispatch_async(dispatch_get_main_queue(), ^{
        [cameraView setImage:image];
    });
}

- (void)destroyDetector;
{
    [detector stop];
}

- (void)createDetector;
{
    // ensure the detector has stopped
    [self destroyDetector];

    // iterate through the capture devices to find the front position camera
    NSArray *devices = [AVCaptureDevice devicesWithMediaType:AVMediaTypeVideo];
    for (AVCaptureDevice *device in devices)
    {
        if ([device position] == AVCaptureDevicePositionFront)
        {
            detector = [[AFDXDetector alloc] initWithDelegate:self
                                                usingCaptureDevice:device
                                                      maximumFaces:1];
            detector.maxProcessRate = 5;

            // turn on all classifiers (emotions, expressions, and emojis)
            [detector setDetectAllEmotions:YES];
            [detector setDetectAllExpressions:YES];
            [detector setDetectEmojis:YES];

            // turn on gender and glasses
            detector.gender = TRUE;
            detector.glasses = TRUE;

            // start the detector and check for failure
            NSError *error = [detector start];

            if (nil != error)
            {
                UIAlertController *alert = [UIAlertController alertControllerWithTitle:@"Detector Error"
                                                                               message:[error localizedDescription]
                                                                        preferredStyle:UIAlertControllerStyleAlert];

                return;
            }

            break;
        }
    }
}

#pragma mark -
#pragma mark AFDXDetectorDelegate Methods

// This is the delegate method of the AFDXDetectorDelegate protocol. This method gets called for:
// - Every frame coming in from the camera. In this case, faces is nil
// - Every PROCESSED frame that the detector
- (void)detector:(AFDXDetector *)detector hasResults:(NSMutableDictionary *)faces forImage:(UIImage *)image atTime:(NSTimeInterval)time;
{
    if (nil == faces)
    {
        [self unprocessedImageReady:detector image:image atTime:time];
    }
    else
    {
        [self processedImageReady:detector image:image faces:faces atTime:time];
    }
}


#pragma mark -
#pragma mark View Methods

- (void)drawRect:(CGRect)rect
{
    cameraView = [[UIImageView alloc] initWithFrame:rect];
    [cameraView setBackgroundColor:[UIColor blueColor]];
    [cameraView setHidden:showCameraProp];
    [self addSubview:cameraView];
    [self createDetector]; // create the dector just before the view appears
}

@end
