#import "React/RCTBridge.h"
#import "RCTFaceManager.h"
#import "RCTFace.h"
#import <UIKit/UIKit.h>

@interface RCTFaceManager() <AFDXDetectorDelegate>
@end

@implementation RCTFaceManager

RCT_EXPORT_MODULE();

RCT_REMAP_VIEW_PROPERTY(showCamera,showCamera, BOOL);
RCT_EXPORT_VIEW_PROPERTY(onChange, RCTBubblingEventBlock)

- (UIView *)view
{
    return [[RCTFace alloc] init];
}

@end
